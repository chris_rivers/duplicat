//
//  TapeDelayFramework.h
//  TapeDelayFramework
//
//  Created by Chris on 16/06/2015.
//  Copyright © 2015 Lofionic. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for TapeDelayFramework.
FOUNDATION_EXPORT double TapeDelayFrameworkVersionNumber;

//! Project version string for TapeDelayFramework.
FOUNDATION_EXPORT const unsigned char TapeDelayFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <TapeDelayFramework/PublicHeader.h>
#import <DuplicatFramework/TapeDelay.h>
