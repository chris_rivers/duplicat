//
//  IAASettings.swift
//  Duplicat
//
//  Created by Chris on 27/06/2016.
//  Copyright © 2016 Lofionic. All rights reserved.
//

import Foundation

let kIAAComponentType:UInt32 = kAudioUnitType_RemoteEffect
let kIAAComponentSubtype:String = "dupl"
let kIAAComponentManufacturer:String = "lfnc"

let kAudiobusKey:String = "MCoqKkR1cGxpY2F0KioqRHVwbGljYXQtMS4zLmF1ZGlvYnVzOi8vKioqW2F1cngubGZuYy5kdXBsLjRd:qS5XUiqX3L+A1Eee/t9CzWzeGkDNywcbazU2PTgr5/sDQ2sAqjwIU+bQOLn1T758FgpcK4GjbOfIxALJBzXGmjn5QTMThM3fiLynb5MRWYoCVtq8Dbyw7E+C39OlGEqQ"
